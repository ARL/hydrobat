const Validator = require('validator');
const isEmpty = require('./is-empty');

/**
 * Validate registration form data
 * @param data
 * @returns {{errors, isValid: *}}
 */
module.exports = function validateRegistration(data) {
  let errors = {}

  data.firstname = !isEmpty(data.firstname) ? data.firstname : '';
  data.lastname = !isEmpty(data.lastname) ? data.lastname : '';
  data.email = !isEmpty(data.email) ? data.email : '';
  data.password = !isEmpty(data.password) ? data.password : '';
  data.password_confirm = !isEmpty(data.password_confirm) ? data.password_confirm : '';
  data.gender = !isEmpty(data.gender) ? data.gender : '';
  data.city = !isEmpty(data.city) ? data.city : '';
  data.age = !isEmpty(data.age) ? data.age : 0;
  data.birthday = !isEmpty(data.birthday) ? data.birthday : '';

  if (!Validator.isLength(data.firstname, {min: 2})) {
    errors.firstname = 'Firstname must be more than 2 chars';
  }

  if (!Validator.isLength(data.lastname, {min: 2})) {
    errors.firstname = 'Lastname must be more than 2 chars';
  }

  if (Validator.isEmpty(data.firstname)) {
    errors.firstname = 'Firstname is required';
  }

  if (Validator.isEmpty(data.lastname)) {
    errors.lastname = 'Lastname is required';
  }

  if (!Validator.isEmail(data.email)) {
    errors.email = 'Email is invalid';
  }

  if (Validator.isEmpty(data.email)) {
    errors.email = 'Email is required';
  }

  if (!Validator.isLength(data.password, {min: 4, max: 30})) {
    errors.password = 'Password must have 4 chars';
  }

  if (Validator.isEmpty(data.password)) {
    errors.password = 'Password is required';
  }

  if (!Validator.isLength(data.password_confirm, {min: 4, max: 30})) {
    errors.password_confirm = 'Password must have 4 chars';
  }

  if (!Validator.equals(data.password, data.password_confirm)) {
    errors.password_confirm = 'Password and confirm password does not match';
  }

  if (Validator.isEmpty(data.password_confirm)) {
    errors.password_confirm = 'Password is required';
  }

  if (Validator.isEmpty(data.gender)) {
    errors.gender = 'Gender is required';
  }

  if (!Validator.equals(data.gender, 'male') && !Validator.equals(data.gender, 'female')) {
    errors.gender = 'Gender must be male or female';
  }

  if (Validator.isEmpty(data.city)) {
    errors.city = 'City is required';
  }

  if (Validator.isEmpty(data.age)) {
    errors.age = 'Age is required';
  }

  if (data.age < 18) {
    errors.age = 'Age is not valid';
  }

  if (!Validator.isNumeric(data.age, {no_symbols: true})) {
    errors.city = 'Age is not valid';
  }

  if (Validator.isEmpty(data.birthday)) {
    errors.birthday = 'Birthday is required';
  }

  return {
    errors,
    isValid: isEmpty(errors)
  }
}