const mongoose = require('mongoose');
const User = mongoose.model('users');

const JWTStrategy = require('passport-jwt').Strategy;
const ExtractJWT = require('passport-jwt').ExtractJwt;

const options = {};

options.jwtFromRequest = ExtractJWT.fromAuthHeaderAsBearerToken();
options.secretOrKey = 'something-very-hard-to-find';

module.exports = passport => {
  passport.use(new JWTStrategy(options, (jwt_payload, done) => {
    User.findById(jwt_payload.id)
        .then(user => {
          if (user) {
            return done(null, user);
          }
          return done(null, false);
        })
        .catch(e => console.error(e));
  }));
}

