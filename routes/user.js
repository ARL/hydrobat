const express = require('express');
const router = express.Router();
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const validateRegistration = require('../validation/register');
const validateLogin = require('../validation/login');

const User = require('../models/User');

router.post('/register', function (request, response) {
  const {errors, isValid} = validateRegistration(request.body);

  if (!isValid) {
    return response.status(400).json(errors);
  }

  User.findOne({
    email: request.body.email
  }).then(user => {
    if (user) {
      return response.status(400).json({
        email: 'Email already exists'
      })
    } else {
      const newUser = new User({
        firstname: request.body.firstname,
        lastname: request.body.lastname,
        email: request.body.email,
        password: request.body.password,
        gender: request.body.gender,
        city: request.body.city,
        age: request.body.age,
        birthday: request.body.birthday
      });

      bcrypt.genSalt(10, (error, salt) => {
        if (error) console.error('There was an error', error);
        else {
          bcrypt.hash(newUser.password, salt, (error, hash) => {
            newUser.password = hash;
            newUser.save().then(user => {
              response.json(user);
            });
          });
        }
      });
    }
  });
});

router.post('/login', (request, response) => {
  const {errors, isValid} = validateLogin(request.body);

  if (!isValid) {
    return response.status(400).json(errors);
  }

  const email = request.body.email;
  const password = request.body.password;

  User.findOne({email})
      .then(user => {
        if (!user) {
          errors.email = 'User not found'
          return response.status(404).json(errors);
        }

        bcrypt.compare(password, user.password)
          .then(ok => {
            if (ok) {
              const payload = {
                id: user.id,
                email: user.email,
                firstname: user.firstname,
                lastname: user.lastname,
                gender: user.gender,
                city: user.city,
                age: user.age,
                birthday: user.birthday
              }
              jwt.sign(payload, 'something-very-hard-to-find', {
                expiresIn: 60*360 // Expires in 360 minutes
              }, (error, token) => {
                if (error) console.error('Token error', error)
                else {
                  response.json({
                    success: true,
                    token: `Bearer ${token}`
                  });
                }
              });
            } else {
              errors.password = 'Email or password incorrect';
              return response.status(400).json(errors);
            }
          });
      });
});

module.exports = router;