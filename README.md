# Test Hydrobat

## Fonctionnalités
- Création d'un compte et enregistrement en base de données
- Connexion avec JWT
- Récupération du token JWT après connexion


## Test
Vérifier si le token JWT est valide sur [JWT.io](https://jwt.io)
 
Secret : ```something-very-hard-to-find```

## Prérequis
- MongoDB
- Node.js
- Nodemon

## MongoDB
Créer une base de données appellée ```hydrobat``` ou changer le fichier ```db.js```.

## Installation

```sh
git clone https://gitlab.com/ARL/hydrobat.git hydrobat
cd hydrobat
yarn
nodemon app.js
```

Aller sur ```localhost:5000```

## Made with
- Express
- MongoDB
- JWT
- Passport
- Validator
- Bootstrap

## Autheur
- Fabien Dartigues
