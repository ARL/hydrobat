const express = require('express');
const bodyParser = require('body-parser');
const path = require('path');
const mongoose = require('mongoose');
const passport = require('passport');
const config = require('./db');

/**
 * User API
 */
const users = require('./routes/user');

/**
 * MongoDB connection
 */
mongoose.connect(config.DB, { useNewUrlParser: true }).then(
    () => {console.log('Database is connected') },
    err => { console.log('Can not connect to the database'+ err)}
);

const app = express();

// Passport
app.use(passport.initialize());
require('./passport')(passport);

// Body parser
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

// serve static files from template
app.use(express.static(__dirname + '/templates'));

// Templates routes
app.get('/', function(req, res) {
  res.sendFile(path.join(__dirname + '/templates/index.html'));
});

app.get('/register', function(req, res) {
  res.sendFile(path.join(__dirname + '/templates/register.html'));
});

app.get('/login', function(req, res) {
  res.sendFile(path.join(__dirname + '/templates/login.html'));
});

// Add login and register post routes
app.use('/', users);

const PORT = 5000;

app.listen(PORT, () => {
  console.log(`Server is running on PORT ${PORT}`);
});